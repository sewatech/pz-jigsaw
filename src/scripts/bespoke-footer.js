// A modified version of bespoke-bullets that supports explicit ordering of steps.
module.exports = function(options) {
  return function(deck) {
    var sections = document.querySelectorAll("section");
    var length = sections.length;
    for (var i = 0; i < length; i++) {
      var section = sections.item(i);

      var footer = document.createElement("footer");
      footer.className="sw";

      var empty = document.createElement("span");
      footer.append(empty);

      var copyright = document.createElement("span");
      copyright.className="copyright";
      copyright.textContent="© Sewatech";
      footer.append(copyright);

      var pageNumber = document.createElement("span");
      pageNumber.className="page-number";
      pageNumber.textContent=i+1;
      footer.append(pageNumber);

      section.append(footer);
    }
  }
};
