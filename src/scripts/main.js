var isWebKit = 'webkitAppearance' in document.documentElement.style,
  // zoom-based scaling causes font sizes and line heights to be calculated differently
  // on the other hand, zoom-based scaling correctly anti-aliases fonts during transforms (no need for layer creation hack)
  scaleMethod = isWebKit ? 'zoom' : 'transform',
  bespoke = require('bespoke'),
  bullets = require('bespoke-bullets'),
  classes = require('bespoke-classes'),
  fullscreen = require('bespoke-fullscreen'),
  hash = require('bespoke-hash'),
  nav = require('bespoke-nav'),
  overview = require('bespoke-overview'),
  scale = require('bespoke-scale');
  toc = require('./bespoke-toc');
  source = require('./bespoke-source');

bespoke.from({ parent: 'article.deck', slides: 'section:not(.skip)' }, [
  classes(),
  nav(),
  fullscreen(),
  (scaleMethod ? scale(scaleMethod) : function(deck) {}),
  // overview({ columns: 4 }),
  bullets('section:not(.no-build) .build, section:not(.no-build) .build-items > *:not(.build-items)'),
  hash()
  , toc()
  , source()
]);
