// A modified version of bespoke-bullets that supports explicit ordering of steps.
module.exports = function(options) {
  return function(deck) {
    deck.slides.forEach(function(slide, s) {
      slide.querySelectorAll('aside.source p')
        .forEach(function(source) {
          var newHTML = source.innerHTML;
          newHTML = newHTML.replace(/\~/g, '\xa0');
          newHTML = newHTML.replace(/\`/g, '\xa0');
          newHTML = newHTML.replace(/ /g, '\xa0');
          newHTML = newHTML.replace("span\xa0", 'span ');
          source.innerHTML = newHTML;
        })
    });
  }
};
