// A modified version of bespoke-bullets that supports explicit ordering of steps.
module.exports = function(options) {
  return function(deck) {
    var tocSection = document.getElementById("toc");

    if (tocSection == null) {
      return;
    }

    var tocTitle = document.createElement("h2");
    tocTitle.textContent = "Plan du cours";
    tocSection.append(tocTitle);

    var toc = document.createElement("ul");
    tocSection.append(toc);

    var tocDetailElement = document.querySelector("#toc-detail aside[role=note]");
    var tocDetailTitle = document.createElement("h2");
    tocDetailTitle.textContent = "Plan détaillé"
    tocDetailElement.append(tocDetailTitle);
    var tocDetail = document.createElement("ul");
    tocDetailElement.append(tocDetail);

    //var sections = document.querySelectorAll("section.title:not(.no-toc)");
    var sections = document.querySelectorAll("section");
    var length = sections.length;
    var lastTitle;

    for (var i = 1; i < length; i++) {
      var section = sections.item(i);

      var title = section.getElementsByTagName("h2").item(0);
      if (title == null || section.className.indexOf("no-toc") != -1) {
        continue;
      }

      var tocEntry = document.createElement("li");

      var tocText = document.createElement("span");
      tocEntry.append(tocText);
      var tocNumber = document.createElement("span");
      tocEntry.append(tocNumber);

      tocText.textContent = title.textContent;
      tocNumber.textContent = i+1;

      if (section.className.indexOf("title") != -1) {
        tocEntry.className = "toc1";
        toc.append(tocEntry);
        tocDetail.append(tocEntry.cloneNode(true));
      } else {
        if (lastTitle == null || title.textContent != lastTitle.textContent) {
          tocEntry.className = "toc2";
          tocDetail.append(tocEntry); 
        }
      }
      lastTitle = title;

    }
  }
}
