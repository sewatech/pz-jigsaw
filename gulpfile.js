'use strict';
var pkg = require('./package.json'),
  autoprefixer = require('gulp-autoprefixer'),
  browserify = require('browserify'),
  buffer = require('vinyl-buffer'),
  chmod = require('gulp-chmod'),
  connect = require('gulp-connect'),
  csso = require('gulp-csso'),
  del = require('del'),
  exec = require('gulp-exec'),
  ghpages = require('gh-pages'),
  gulp = require('gulp'),
  gutil = require('gulp-util'),
  jade = require('gulp-jade'),
  path = require('path'),
  plumber = require('gulp-plumber'), // plumber prevents pipe breaking caused by errors thrown by plugins
  rename = require('gulp-rename'),
  source = require('vinyl-source-stream'),
  stylus = require('gulp-stylus'),
  through = require('through'),
  uglify = require('gulp-uglify'),
  isDist = process.argv.indexOf('deploy') >= 0,
  cleanCSS = require('gulp-clean-css'),
  uglify = require('gulp-uglify'),
  svgmin = require('gulp-svgmin');

var MAX_HTML_FILE_SIZE = 20000; // kbytes

gulp.task('js', ['clean:js'], function() {
  // see https://wehavefaces.net/gulp-browserify-the-gulp-y-way-bb359b3f9623
  return browserify('src/scripts/main.js').bundle()
    // NOTE this error handler fills the role of plumber() when working with browserify
    .on('error', function(e) { if (isDist) { throw e; } else { gutil.log(e.stack); this.emit('end'); } })
    .pipe(source('src/scripts/main.js'))
    .pipe(buffer())
    .pipe(isDist ? uglify() : through())
    .pipe(rename('build.js'))
    .pipe(gulp.dest('public/build'))
    .pipe(connect.reload());
});


gulp.task('html', ['clean:html'], function() {
  return gulp.src('src/index.adoc')
    .pipe(isDist ? through() : plumber())
    .pipe(exec('bundle exec asciidoctor-bespoke -o - src/index.adoc', { pipeStdout: true, maxBuffer: MAX_HTML_FILE_SIZE*1024 }))
    .pipe(exec.reporter({ stdout: false }))
    .pipe(rename('index.html'))
    .pipe(chmod(644))
    .pipe(gulp.dest('public'))
    .pipe(connect.reload());
  // return gulp.src('src/index.adoc')
  //   .pipe(isDist ? through() : plumber())
  //   .pipe(exec('bundle exec asciidoctor-bespoke -o public/index.html src/index.adoc'))
  //   .pipe(connect.reload());

});

gulp.task('css', ['clean:css'], function() {
  return gulp.src('src/styles/main.styl')
    .pipe(isDist ? through() : plumber())
    .pipe(stylus({ 'include css': true, paths: ['./node_modules'] }))
    .pipe(autoprefixer({ browsers: ['last 2 versions'], cascade: false }))
    .pipe(isDist ? csso() : through())
    .pipe(rename('build.css'))
    .pipe(gulp.dest('public/build'))
    .pipe(connect.reload());
});

gulp.task('fonts', ['clean:fonts'], function() {
  return gulp.src(['src/fonts/**/*',
                   'node_modules/font-awesome/fonts/**/*' , 'node_modules/font-awesome/css/font-awesome.css'
                   ])
    .pipe(gulp.dest('public/fonts'))
    .pipe(connect.reload());
});

gulp.task('images:svg', ['clean:images'], function() {
  return gulp.src('src/images/**/*.svg')
    .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
    .pipe(gulp.dest('public/images'))
    .pipe(connect.reload());
});

gulp.task('images', ['images:svg'], function() {
  return gulp.src(['src/images/**/*', '!src/images/**/*.svg'])
    .pipe(gulp.dest('public/images'))
    .pipe(connect.reload());
});

gulp.task('hljs', ['clean:hljs', 'hljs:js', 'hljs:css']);

gulp.task('hljs:js', ['clean:hljs'], function() {
  return gulp.src('node_modules/highlightjs/highlight.pack.js')
    .pipe(uglify())
    .pipe(rename('highlight.min.js'))
    .pipe(gulp.dest('public/hljs'))  
    .pipe(connect.reload());
});
gulp.task('hljs:css', ['clean:hljs'], function() {
  return gulp.src('node_modules/highlightjs/styles/*.css')
    .pipe(cleanCSS())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('public/hljs/styles'))
    .pipe(connect.reload());
});

gulp.task('clean', function() {
  return del('public');
});

gulp.task('clean:html', function() {
  return del('public/index.html');
});

gulp.task('clean:js', function() {
  return del('public/build/build.js');
});

gulp.task('clean:css', function() {
  return del('public/build/build.css');
});

gulp.task('clean:fonts', function() {
  return del('public/fonts');
});

gulp.task('clean:images', function() {
  return del('public/images');
});

gulp.task('clean:hljs', function() {
  return del('public/hljs');
});

gulp.task('connect', ['build'], function() {
  connect.server({ root: 'public', port: 8000, livereload: true });
});

gulp.task('watch', function() {
  gulp.watch('src/**/*.adoc', ['html']);
  gulp.watch('src/scripts/**/*.js', ['js']);
  gulp.watch('src/styles/**/*.styl', ['css']);
  gulp.watch('src/images/**/*', ['images']);
  gulp.watch('src/fonts/*', ['fonts']);
});

gulp.task('deploy', ['clean', 'build'], function(done) {
  ghpages.publish(path.join(__dirname, 'public'), { logger: gutil.log }, done);
});

gulp.task('build', ['js', 'html', 'css', 'fonts', 'images', 'hljs']);
gulp.task('serve', ['connect', 'watch']);
gulp.task('default', ['build']);
